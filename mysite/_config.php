<?php

global $project;
$project = 'mysite';

global $database;
$database = 'openstack_scratchpad';

Director::set_environment_type('dev');
Security::setDefaultAdmin('root','root');

error_reporting(E_ALL);
ini_set('display_errors', 1);
// Set the site locale
i18n::set_locale('en_US');

global $databaseConfig;
$databaseConfig = array(
    "type" => 'SQLite3Database',
    "server" => 'none',
    "username" => 'none',
    "password" => 'none',
    "database" => $database,
    "path" => __DIR__."/db"
);