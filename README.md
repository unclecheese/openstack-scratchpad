# Openstack Scratchpad

Zero-configuration prototyping for openstack.org.

## How it works

Using the [silverstripe-green](http://github.com/unclecheese/silverstripe-green) module, this project provides a clean slate for injecting new designs into the www.openstack.org layout without the need to write any code or make database mutations. Combining PHP's native server, SQLite, and WebpackDevServer, a full-stack environment is configured and launchable from within this package.

## Installation

```
git clone git://gitlab.com/unclecheese/openstack-scratchpad
cd openstack-scratchpad
./install
```

## Usage

The `scratchpad` application that gets installed globally contains everything you need.

### Creating a new design

`$ scratchpad create my-design-name`

Creates a new subfolder in the `_designs` using the design name provided. It will automatically update the `webpack.config.js`, add sample content, a sample `index.ss` file, and an empty `.scss` file to get you started.

#### Options

To create a new design and launch it in the browser, append `--launch` to this command

### Launching an existing design

`$ scratchpad launch my-design-name`

Starts a PHP development server and a Webpack development server, and launches the given design in a new browser window. SCSS and Javascript are live reloaded.

### Exporting the design to the main site (coming soon)

`$ scratchpad export my-design-name /path/to/my/www.openstack.org/`

Exports a design to a module (or theme) of openstack.org proper.

#### Options

`--generate` Convert all the "green" code to custom PHP classes

`-parent=[page_name or page_id]` If exporting the design module in its "green" form, provide a node of the site tree where it will appear on the main site.

`-module=[module_name]` Module to export to (default is themes/openstack)

