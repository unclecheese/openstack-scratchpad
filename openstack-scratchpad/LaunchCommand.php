<?php

namespace Openstack\Scratchpad;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Style\SymfonyStyle;


class LaunchCommand extends Command
{

	protected $browserOpen = false;


	protected function configure()
	{
		$this->setName('launch')
			  ->setDescription('Launches an existing design module')
			  ->addArgument(
			  	'module',
			  	InputArgument::REQUIRED,
			  	'The name of the module to launch'
			  );
	}


	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$io = new SymfonyStyle($input, $output);
		$fs = new FileSystem();

		$moduleName = $input->getArgument('module');

		$socket = @fsockopen('localhost', 8080, $errno, $errstr, 1);
        if($socket) {
        	$result = $io->confirm("A PHP dev server already appears to be running. Would you like to use it? (If you've been using it on other projects, say no)", true);
			if(!$result) {
				$pid = @file_get_contents(__DIR__.'/.pid');
				if(!$pid) {
					return $io->error("No .pid file was found. Try finding the 'php' process in Activity Monitor and force-quit it.");
				}

				$kill = new Process("kill $pid");
				$kill->run();
				if(!$kill->isSuccessful()) {
					return $io->error("Unable to kill PHP server. Try finding the 'php' process in Activity Monitor and force-quit it.");
				}
			}
        }
        else {
	        $phpServer = new Process('vendor/bin/serve');
	        $phpServer->start();
	        $pid = $phpServer->getPid();
	        $fs->dumpFile(
	        	__DIR__.'/.pid',
	        	$pid
	        );
	        $io->text("PHP server running on http://localhost:8080 (process $pid)");
        }

		$socket = @fsockopen('127.0.0.1', 3000, $errno, $errstr, 1);
        if($socket) {
        	$result = $io->confirm("A webpack dev server already appears to be running for a different project. You'll have to shut it down. Kill it now?", true);
			if($result) {
				$kill = new Process('killall node');
				$kill->run();
				if(!$kill->isSuccessful()) {
					return $io->error("Unable to kill webpack dev server. Try finding the 'node' process in Activity Monitor and force-quit it.");
				}
			}
			else {
				return $io->text('Canceling.');
			}
        }


        $devServer = new Process('node ./webpack _designs -t=serve');
        $devServer->start();
		$io->text("Webpack dev server running on http://127.0.0.1:3000");

        $self =& $this;
		$devServer->wait(function ($type, $buffer) use ($moduleName, $self) {
		    if (Process::ERR === $type) {
		        echo 'ERR > '.$buffer;
		    } 
		    else if(!$self->isBrowserOpen()) {			    	
		        if(!preg_match('/^webpack: bundle is now VALID/', $buffer)) {			        	
		        	$browser = new Process("open http://localhost:8080/$moduleName");
		        	$browser->start();
		        	$self->setBrowserOpen(true);
		        }
		    }
		});

		$io->text("Module launched at http://localhost:8080/$moduleName...");


	}


	public function setBrowserOpen($bool)
	{
		$this->browserOpen = $bool;
	}


	public function isBrowserOpen()
	{
		return $this->browserOpen;
	}
}