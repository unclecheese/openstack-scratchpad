<?php

namespace Openstack\Scratchpad;

use Symfony\Component\Console\Application as BaseApplication;

class Application extends BaseApplication
{
    /**
     * Set the default commands
     * @return array
     */
    protected function getDefaultCommands()
    {
        $commands = parent::getDefaultCommands();
        $commands[] = new CreateCommand();
        $commands[] = new LaunchCommand();

        return $commands;
    }	
}