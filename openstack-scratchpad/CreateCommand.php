<?php

namespace Openstack\Scratchpad;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateCommand extends Command
{
	protected function configure()
	{
		$this->setName('create')
			  ->setDescription('Creates a new design module')
			  ->addArgument(
			  	'module',
			  	InputArgument::REQUIRED,
			  	'The name of the module to create'
			  )
			  ->addOption(
			  	'launch',
			  	'l',
			  	InputOption::VALUE_NONE,
			  	false
			  );
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$fs = new Filesystem();
		$io = new SymfonyStyle($input, $output);
		$moduleName = $input->getArgument('module');
		$moduleDir = __DIR__.'/../_designs/'.$moduleName;
		$configPath = __DIR__.'/../_designs/webpack.config.js';

		if(is_dir($moduleDir)) {
			$result = $io->confirm("Module $moduleName already exists. Delete it?", false);
			if($result) {
				$fs->remove($moduleDir);
			}
			else {
				return $io->warning('Module not created.');				
			}
		}

		$contents = @file_get_contents($configPath);		
		if(!$contents) {
			return $io->error("Unable to read $configPath");			
		}

		preg_match('/entry: (\{.*?\}),/s', $contents, $matches);
		if(!$matches) {
			return $io->error("$configPath config not formatted correctly");
		}	

		$arr = json_decode($matches[1], true);
		$arr[$moduleName] = "$moduleName/$moduleName.js";
		$newObj = json_encode($arr, JSON_PRETTY_PRINT);
		$newObj = str_replace("\n    \"", "\n\t\t\"", $newObj);
		$newObj = str_replace("}", "\t}", $newObj);		
        $newObj = str_replace('\/','/',$newObj);

		$contents = preg_replace(
			'/entry: \{.*?\},/s',
			"entry: {$newObj},",
			$contents
		);
		
		$fs->dumpFile($configPath, $contents);

		$fs->mkdir($moduleDir);
		$this->buildModuleDirectory($moduleDir, $moduleName);

		$io->success("Module $moduleName created successfully.");

		if($input->getOption('launch')) {
			$launchCmd = $this->getApplication()->find('launch');
			$arguments = [
				'module' => $moduleName
			];
			$launchInput = new ArrayInput($arguments);
			$launchCmd->run($launchInput, $output);			
		}
	}

	protected function buildModuleDirectory($moduleDir, $moduleName)
	{
		$fs = new Filesystem();

		$fs->dumpFile(
			"$moduleDir/{$moduleName}.js",
"import './$moduleName.scss'\n\n"
		);

		$fs->dumpFile(
			"{$moduleDir}/{$moduleName}.scss",
"/* $moduleName styles */\n\n\n"
		);

		$fs->dumpFile(
			"$moduleDir/index.ss",
"\$WebpackJS('_designs/_builds', '${moduleName}.js')\n`
<h2>Welcome to \$ModuleName</h2>"
		);

		$fs->dumpFile(
			"$moduleDir/content.yml",
"# Custom fields\n
ModuleName: {$moduleName}\n"
		);

		$fs->dumpFile(
			"$moduleDir/config.yml",
			"public_url: {$moduleName}"
		);

	}
}
