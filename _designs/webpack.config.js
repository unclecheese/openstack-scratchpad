var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: {
		"some-new-thing": "some-new-thing/some-new-thing.js",
		"foo": "foo/foo.js"
	},

	output: {
		filename: '[name].js',
		path: path.join(__dirname, '_builds'),
		publicPath: path.join('/', path.basename(__dirname),'_builds/')
	}
};